# Interview Tasks

## Purpose

This is a set of challenges to be used during interview process (head hunting/interviewing candidates).

## Tips

* Every task contains readme file detailing results expected.
* The readme file also contains some tips that could potentially help resolving the problem given.
* There's also a legend detailing what should/can/cannot be changed while working on tasks.
* All tasks can be resolved using modern browsers (e.g. Google Chrome)

## Tasks

* Keypress
* Wrap
* Times
* Transform
* Equality
* Uniqueness
