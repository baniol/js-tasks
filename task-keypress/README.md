# Keypress Task

## Purpose

The task is about optimising number of ajax calls being made to the server.

## Tips

* When user types 30 letters in the search box, 30 ajax requests are being made to the server. This is inefficient. Please find a way to optimise this.
* Ajax calls are being automatically counted and displayed in the HTML file (e.g. "Number of ajax calls made: 5").

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```