# Transform Task

## Purpose

The task is about filtering a collection and transforming data so we can display partial/transformed data on the page.

## Tips

* `API.filter` function must return a collection (array)
* `API.transform` function must return a collection (array)
* Please implement these two functions above so only Goalkeepers are displayed on the page

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```