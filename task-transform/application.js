/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (API, $) {
  'use strict';

  /**
   * Collection of player data (position, firstName, lastName)
   *
   * @private
   */
  var playersData = [
    { position : 'Goalkeeper', firstName : 'Petr',      lastName: 'Cech' },
    { position : 'Goalkeeper', firstName : 'Mark',      lastName: 'Schwarzer' },
    { position : 'Defender',   firstName : 'Branislav', lastName: 'Ivanovic' },
    { position : 'Defender',   firstName : 'John',      lastName: 'Terry' },
    { position : 'Defender',   firstName : 'Gary',      lastName: 'Cahill' },
    { position : 'Defender',   firstName : 'Ashley',    lastName: 'Cole' }
  ];

  /**
   * Prints squad in [data-role="players-list"] element
   *
   * @public
   */
  API.printGoalkeepers = function () {
    $('[data-role="players-list"]').html( API.transform(API.filter(playersData)).join('<br>') );
  };

}(this, this.jQuery));



// [Can be modified]
(function (API) {
  'use strict';

  /**
   * Returns collection containing Goalkeepers only
   *
   * @public
   */
  API.filter = function (collection) {
    return collection.filter(function (value) {
      // if (value.position === 'Goalkeeper') {
      //   return value;
      // }
      return value.position === 'Goalkeeper';
        
    });
 
    // return collection; // your implementation here
  };

  /**
   * Returns collection containing '(<position>) <firstName> <lastName>' strings
   *
   * @public
   */
  API.transform = function (collection) {

    return collection.map(function (elem) {
      return '(' + elem.position + ') ' + elem.firstName + ' ' + elem.lastName;
    });

    // return collection; // your implementation here
  };

  /**
   * Prints Goalkeepers in the source of the page
   *
   * @public
   */
  API.printGoalkeepers();
}(this));
