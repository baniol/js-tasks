document.addEventListener('DOMContentLoaded', function () {
  init();
});

var addItemButton;
var itemName;
var items;

var startItems = [
  {name: 'one', id: 'il2nk563su37syvi'},
  {name: 'two', id: 'zvrre3ia4zmbcsor'}
];

function getItems () {
  return JSON.parse(localStorage.getItem('items'));
}

function saveItems (items) {
  localStorage.setItem('items', JSON.stringify(items));
}

function init () {
  addItemButton = document.querySelector('#add-item');
  itemName = document.querySelector('#item-name');

  items = getItems();
  if (!items) saveItems(startItems);

  buildList();
  document.querySelector('#items-wrapper').addEventListener('click', clickItem);
  addItemButton.addEventListener('click', addItem);
}

function buildList () {
  for (var i = 0; i < items.length; i++) {
    var item = items[i];
    var div = createDiv(item.name, item.id);
    document.querySelector('#items-wrapper').appendChild(div);
  }
}

function createDiv (text, id) {
  var div = document.createElement('div');
  var t = document.createTextNode(text);
  div.id = id;
  div.className = 'item';
  div.appendChild(t);
  return div;
}

function getRandomId () {
  return Math.random().toString(36).slice(2);
}

function clickItem (e) {
  var element = e.target;
  var text;
  if (/opened/.test(element.className)) {
    text = element.innerHTML.split(' : ')[0];
    element.className = element.className.replace(' opened', '');
  }
  else {
    text = element.innerHTML + ' : ' + element.id;
    element.className = element.className + ' opened';
  }
  element.innerText = text;
  
}

function addItem () {
  var newItem = {name: itemName.value, id: getRandomId()};
  items.push(newItem);
  saveItems(items);
  var name = itemName.value;
  var div = createDiv(newItem.name, newItem.id);
  document.querySelector('#items-wrapper').appendChild(div);
  itemName.value = '';
  displayItems();
}

function displayItems () {
  console.log(items);
}