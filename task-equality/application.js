/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (API, $) {
  'use strict';

  /**
   * Collection of player data (position, firstName, lastName)
   *
   * @private
   */
  var firstObject  = { position : 'Goalkeeper', firstName : 'Petr', lastName: 'Cech' };
  var secondObject = { position : 'Goalkeeper', firstName : 'Mark', lastName: 'Schwarzer' };
  var thirdObject  = { position : 'Goalkeeper', firstName : 'Petr', lastName: 'Cech' };



  /**
   * Prints comparison results
   *
   * @public
   */
  API.printComparisonResults = function () {
    var results;

    results = [
      API.areEqual(firstObject,  secondObject),
      API.areEqual(secondObject, thirdObject),
      API.areEqual(thirdObject,  firstObject),
    ];

    results = results.map(function (result) {
      if (result) {
        return 'Both objects are equal';
      }
      return 'Objects are not equal';
    });

    $('[data-role="comparison-results"]').html( results.join('<br>') );
  };

}(this, this.jQuery));



// [Can be modified]
(function (API) {
  'use strict';

  /**
   * Returns true if objects have the same structure and values. Returns false otherwise
   *
   * @public
   */
  API.areEqual = function (firstObject, secondObject) {

    ////SOLUTION 1

    // var arrKeys = Object.keys(firstObject);

    // return arrKeys.every(function(element, index, array) {
    //   return firstObject[element] === secondObject[element];
    // }); 

    ////SOLUTION 2

    return JSON.stringify(firstObject) === JSON.stringify(secondObject); 

    // your implementation here;
  };

  /**
   * Prints comparison results in the page source
   *
   * @public
   */
  API.printComparisonResults();
}(this));
