/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (API, $) {
  'use strict';

  /**
   * Collection Class
   *
   * Sample Usage:
   *
   *   var collection = new Collection (1,2,3) // => Collection ( items: [1,2,3], add: function );
   *   collection.add(5,6,7) // => Collection ( items: [1,2,3,5,6,7], add: function );
   *
   * @public
   */
  API.Collection = (function () {

    // @constructor
    function Collection (/* arg1, arg2, ..., argn */) {
      this.items = Array.apply(this, arguments);
    }

    // @method
    Collection.prototype.add = function (/* arg1, arg2, ..., argn */) {
      Array.prototype.push.apply(this.items, arguments);

      return this;
    };

    return Collection;
  }());


  /**
   * Prints test results
   *
   * @public
   */
  API.printTestResults = function () {
    var messages = [];

    var collection = new API.Collection(1,2,3);
    messages.push('Collection size: ' + collection.size);

    collection.add(5,6,7);
    messages.push('Collection size: ' + collection.size);

    $('[data-role="test-results"]').html(messages.join('<br>'));
  };

}(this, this.jQuery));



// [Can be modified]
(function (API) {
  'use strict';

  // @TODO: Your implemention of <CollectionObject>.size here

  /**
   * Prints test results
   *
   * @public
   */
  API.printTestResults();
}(this));
