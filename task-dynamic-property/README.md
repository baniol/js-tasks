# Dynamic Property

## Purpose

The task is about creating dynamically computed property to an object.

## Tips

* Key point here is to make sure `size` is a simple property rather than a function. In other words - you should use `<object>.size` rather than `<object>.size()`
* Please try to avoid extending `API.Collection.prototype.add` method

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```