# Wrap Task

## Purpose

The task is about extending 3rd party API you are forced to use and adding your own functionalities on top of that.

## Tips

* The player list should contain '(<position>)' prefixing player name, e.g. "(Goalkeeper) Petr Cech" instead of "Petr Cech".
* You can only use the modifiable block of code in `application.js` in order to achieve this.
* You should try to avoid copying & pasting code. Keep in mind that 3rd party code can change. You should re-use it instead.

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```