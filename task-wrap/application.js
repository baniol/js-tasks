/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (API, $) {
  'use strict';

  /**
   * Collection of player data (position, firstName, lastName)
   *
   * @private
   */
  var playersData = [
    { position : 'Goalkeeper', firstName : 'Petr',      lastName: 'Cech' },
    { position : 'Defender',   firstName : 'Branislav', lastName: 'Ivanovic' },
    { position : 'Defender',   firstName : 'John',      lastName: 'Terry' },
    { position : 'Defender',   firstName : 'Gary',      lastName: 'Cahill' },
    { position : 'Defender',   firstName : 'Ashley',    lastName: 'Cole' }
  ];

  /**
   * Translates player object to '<firstName> <LastName>' string
   *
   * @public
   */
  API.playerDisplayName = function (player) {
    return player.firstName + ' ' + player.lastName;
  };

  /**
   * Joins player display names with <br> tags so we can print them in HTML
   *
   * @public
   */
  API.printableSquadInformation = function (players) {
    return players.map(API.playerDisplayName).join('<br>');
  };

  /**
   * Prints squad in [data-role="players-list"] element
   *
   * @public
   */
  API.printSquad = function () {
    $('[data-role="players-list"]').html( API.printableSquadInformation(playersData) );
  };

}(this, this.jQuery));



// [Can be modified]
(function (API) {
  'use strict';
  // Your code here...

// //  SOLUTION 1

//   var funcMy = API.printableSquadInformation;

//   API.printableSquadInformation = function (players) {
//     players.map(function(person, index) {
//       players[index].firstName = '(' + players[index].position + ') ' + players[index].firstName;
//     });
//     return funcMy(players);
//   };


//  SOLUTION 2

  var fun = API.playerDisplayName;
  API.playerDisplayName = function (person) {
    return'(' + person.position + ') ' + person.firstName + ' ' + person.lastName;
  };

 


  API.printSquad();
  console.log(API);
}(this));


  console.log(this);
