/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Can be modified]
(function (global, $) {
  'use strict';

  Number.prototype.times = function (fun) {
  	for (var startVal = 1; startVal <= this; startVal++) {
      fun(startVal);
    }
  };

  // Your code here

}(this, this.jQuery));


// [Cannot be modified]
(function (global, $) {
  'use strict';

  (5).times(function (number) {
    $('[data-role="iterations-list"]').append('Iteration ' + number + '<br>');
  });

}(this, this.jQuery));
